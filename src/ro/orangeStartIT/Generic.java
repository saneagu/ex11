package ro.orangeStartIT;

public class Generic<Employee>{
    // A public class called Generic that will contain one parameter Employee
    public Employee empl; //Declare an object of type Employee

    public Generic(Employee empl){
        this.empl = empl;
    }//A constructor for Employee

    public Employee getEmpl() {
        return empl;
    }
}
