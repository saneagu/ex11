package ro.orangeStartIT;

public class TestGeneric {

    public static void main(String[] args) {

    Generic<String> S1 = new Generic<>("Samuel Jackson");
    Generic<String> S2 = new Generic<>("Plazza Hotel");

    Generic<Integer> salary = new Generic<>(2000);

    System.out.println("My name is " + S1.getEmpl() + ". I work as cashier at " + S2.getEmpl());
    System.out.println("My salary is valued somewhere around " + salary.getEmpl() + " dollars.");

    }
}
